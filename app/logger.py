#!/usr/bin/env python
"""
packet_logger

PyQt5 application which receives the UPD packets, adds its own timestamp
of packet reception, and logs it to that packet's respective file.
"""

import json
import random
import sys
from time import sleep

from PyQt5.QtCore import *
from PyQt5.QtNetwork import *
from PyQt5.QtWidgets import *

from packets import HEADER_STRUCT, get_packets


class PacketLogger(QWidget):

    def __init__(self):
        super(PacketLogger, self).__init__()

        self.packets = get_packets('packets.json')
        # log files hack
        self.files = [None] * len(self.packets)
        for packet in self.packets:
            if packet is not None:
                self.files[packet.identifier] = open('logs/' + packet.log_file, 'a')

        # bind callback for UDP packets
        self.socket = QUdpSocket(self)
        self.socket.bind(QHostAddress('0.0.0.0'), 8888)
        self.socket.readyRead.connect(self.readPendingDatagrams)

        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 290, 150)
        self.setWindowTitle('Logger')
        self.show()

    def readPendingDatagrams(self):
        while self.socket.hasPendingDatagrams():
            data, _, _ = self.s.readDatagram(self.socket.pendingDatagramSize())
            self.processDatagram(data)
        self.update()

    def processDatagram(self, datagram):
        # make header size also modular
        firmware_version, identifier = HEADER_STRUCT.unpack(datagram[0:2])
        if packets[identifier % 128] is not None:
            try:
                # check whether RX diag is present
                if (identifier / 128) >= 1:
                    contents = [0] * len(packet.contents_diag)
                    contents = packet.struct_diag.unpack(datagram[2:])
                else:
                    contents = [0] * len(packet.contents)
                    contents = packet.struct.unpack(datagram[2:])
                print(identifier % 128, packet.name, contents)
                self.files[identifier].write('%s\n' % (str(contents)))
            except:
                print("Packet ID %i with size %i didn't match specifications" %
                      (identifier % 128, len(datagram) - 2))


def main():

    app = QApplication(sys.argv)
    ex = PacketLogger()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
