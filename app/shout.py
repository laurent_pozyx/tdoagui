import socket
import struct
from random import randint
from time import sleep, time


def shout(queue):
    struc = struct.Struct(">hIii")
    anchorId = [0x1001, 0x1002, 0x1003, 0x1004, 0x1005, 0x1006]

    freq_total = 10
    sets = 2
    time_delay = 1 / (freq_total * sets)

    i = 0
    while True:
        i += 1
        sleep(time_delay)
        x = [randint(2600, 2700), randint(1900, 2100), randint(400, 600), randint(
            8000, 8200), randint(5500, 5700), randint(7900, 8100)]
        y = [randint(4000, 4300), randint(2900, 3100), randint(4600, 4800), randint(
            3500, 3900), randint(3500, 3900), randint(1800, 2000)]
        data = {"Packet type": 'tag', "tag_id": anchorId[i % 6], "x": x[i % 6], "y": y[i % 6], "z": 0, "a_x": 0,
                "a_y": 0, "a_z": 0, "sigma": 0, "rss": 0, "update_rate": 1, "packet_loss": 0.2,
                "anchor_times": {0x00A1: 0.0001, 0x000A2: 0.0003, 0x00A3: 0.0001, 0x00A4: 0.0001}}
        queue.put(data)
