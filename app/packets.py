"""
Initializes the Packets correctly from the JSON and thus
separates this from the GUI itself. This can be used as a library
to return 'Packet' objects read from a packets.json file.

This requires/assumes the packets.json file as the only one that can be changed
on a per-test basis. The other files (packet_contents.json, payloads.json,
and payload_contents.json) are formalized, and are better left alone once defined.

The payload is also opened up but this would preferably remain unused.

Better standards for better coding (sorry)
"""

import copy
import json
from struct import Struct

DEFINITIONS_FOLDER = ""
HEADER_STRUCT = Struct('>BB')
RX_DIAGNOSTICS_CONTENTS = ["RX_DIAGNOSTICS"]  # fill with content like currently done but with split


def get_payload_contents():
    with open(DEFINITIONS_FOLDER + 'payload_contents.json', 'r') as fp:
        return json.load(fp)


def get_packet_contents():
    with open(DEFINITIONS_FOLDER + 'packet_contents.json', 'r') as fp:
        return json.load(fp)


def get_payload_definitions(filename=None):
    if filename is None:
        filename = 'payloads.json'
    with open(DEFINITIONS_FOLDER + filename, 'r') as fp:
        return json.load(fp)


def get_packet_definitions(filename=None):
    if filename is None:
        filename = 'packets.json'
    with open(DEFINITIONS_FOLDER + filename, 'r') as fp:
        return json.load(fp)


class Packet(object):
    """Defines a packet, with its data size and format"""

    def __init__(self, name, metadata):
        self.name = name
        self.log_file = metadata['log_file']
        self.identifier = metadata['identifier']

        self.contents = metadata['contents']
        self.payloads = metadata['payload']
        self.description = metadata['description']

        self.construct_packet_data_structs()

    def construct_packet_data_structs(self):
        # maybe rename to packet_contents_definitions
        packet_contents = get_packet_contents()
        self.contents_diag = RX_DIAGNOSTICS_CONTENTS + self.contents
        self.data = '>'
        for content in self.contents:
            self.data += packet_contents[content]
        self.struct = Struct(self.data)
        self.data_diag = '>'
        for content in self.contents_diag:
            self.data_diag += packet_contents[content]
        self.struct_diag = Struct(self.data_diag)

    # still not sure whether to call this from init and pass payload definitions there
    def add_payload_data_structs(self, all_payloads):
        for packet_payload in self.payloads.split(','):
            for payload_definition in all_payloads:
                if packet_payload == payload_definition.name:
                    self.data += payload_definition.data
                    self.data_diag += payload_definition.data
                    self.contents += payload_definition.contents
                    self.contents_diag += payload_definition.contents

        self.struct = Struct(self.data)
        self.struct_diag = Struct(self.data_diag)


# making this class is pretty overkill and might be changed in the future.
class Payload(object):
    """Defines a payload, with its data size and format"""

    def __init__(self, name, metadata):
        self.name = name
        self.contents = metadata['contents']
        self.description = metadata['description']

        self.construct_payload_data_struct()

    def construct_payload_data_struct(self):
        payload_contents = get_payload_contents()
        self.data = ''
        for content in self.contents:
            self.data += payload_contents[content]


def get_payloads(filename=None):
    payload_definitions = get_payload_definitions(filename)
    payloads = []
    for payload_definition in payload_definitions:
        payloads.append(Payload(payload_definition, payload_definitions[payload_definition]))
    return payloads


def sort_packets(packets):
    sorted_packets = [None] * 128
    for packet in packets:
        sorted_packets[packet.identifier] = copy.deepcopy(packet)
    return sorted_packets


def get_packets(packets_filename=None, payloads_filename=None):
    packet_definitions = get_packet_definitions(packets_filename)
    payloads = get_payloads(payloads_filename)
    packets = []
    for packet_definition in packet_definitions:
        packets.append(Packet(packet_definition, packet_definitions[packet_definition]))
        packets[-1].add_payload_data_structs(payloads)
    return sort_packets(packets)


if __name__ == '__main__':
    packets = get_packets()
    for packet in packets:
        print(packet.name, packet.identifier, packet.contents, packet.data)
