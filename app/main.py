from multiprocessing import Process, Queue
import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from TDOAGui import TdoaMainWindow
from shout import shout

def spawnGui(queue):
   app = QApplication(sys.argv)
   ex = TdoaMainWindow(queue)
   ex.show()
   sys.exit(app.exec_())

if __name__ == '__main__':
    q = Queue()

    p1 = Process(target=shout, args=(q,))
    p1.start()

    p2 = Process(target=spawnGui, args=(q,))
    p2.start()
