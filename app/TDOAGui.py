import sys, os
import random
import time, threading
from time import sleep
import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np

import PyQt5
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtNetwork import *

import socket
import struct
from collections import OrderedDict
from multiprocessing import Process, Queue

class TdoaMainWindow(QMainWindow):
    def __init__(self, queue, parent = None):
        super(TdoaMainWindow, self).__init__(parent)
        self.queue = queue
        self.anchor_dict = {0x00A1: [8.369, 0], 0x00A2: [0, 0], 0x00A3: [0, 5.614], 0x00A4: [8.369, 5.614]}
        self.tag_dict = {}


        self.tabbing_window =  QTabWidget(self)
        self.setCentralWidget(self.tabbing_window)
        self.positioning_window = PositioningWindow(queue, self)
        self.diagnostics_window = DiagnosticsWindow(queue, self)
        self.tabbing_window.addTab(self.positioning_window, "Positioning")
        self.tabbing_window.addTab(self.diagnostics_window, "Diagnostics")

        # bar = self.menuBar()
        # file = bar.addMenu("File")
        # file.addAction("New")
        # file.addAction("Save")
        # file.addAction("Quit")

        self.setWindowTitle("TDOA visualization")
        self.showMaximized()
        self.show()

        timer_timeout = 0.001
        self.timer = QTimer(self)
        self.timer.setInterval(timer_timeout)
        self.timer.timeout.connect(self.update)
        self.timer.start()

    def update(self):
        self.readPendingDatagrams()

    def addTag(self, tag):
        self.tag_dict[tag.id] = tag
        self.positioning_window.tag_listwidget_items[tag.id] = QListWidgetItem(hex(tag.id))
        self.positioning_window.tag_listwidget.addItem(self.positioning_window.tag_listwidget_items[tag.id])
        self.positioning_window.tag_listwidget_items[tag.id].setCheckState(False)
        self.positioning_window.tag_listwidget.itemClicked.connect(self.updateTagInfo)

        self.diagnostics_window.tag_listwidget_items[tag.id] = QListWidgetItem(hex(tag.id))
        self.diagnostics_window.tag_listwidget.addItem(self.diagnostics_window.tag_listwidget_items[tag.id])
        self.diagnostics_window.tag_listwidget_items[tag.id].setCheckState(False)


    def removeTag(self, tag_id):
        del self.tag_dict[tag_id]
        self.positioning_window.tag_listwidget.removeItemWidget(self.positioning_window.tag_listwidget_items[tag_id])
        del self.positioning_window.tag_listwidget_items[tag_id]

    def updateTagInfo(self):
        for tag_id, item in self.positioning_window.tag_listwidget_items.items():
            tag = self.tag_dict[tag_id]
            tag.setVisible(item.checkState())
            if item.isSelected():
                for property_name in self.positioning_window.tag_info_dict:
                    if property_name == "Tag ID":
                        self.positioning_window.tag_info_treewidget_items[property_name].setText(0, hex(tag.id))
                    elif property_name == "Anchor timestamps":
                        formatted_dict  = ""
                        for anchor_id, timestamp in tag.properties[property_name].items():
                            formatted_dict += hex(anchor_id) + ": " + str(timestamp) + " \n"
                        self.positioning_window.tag_info_treewidget_items[property_name].setText(0, formatted_dict)
                    else:
                        self.positioning_window.tag_info_treewidget_items[property_name].setText(0, str(tag.properties[property_name]))

    def readPendingDatagrams(self):
        if not self.queue.empty():
            data = self.queue.get_nowait()
            self.processDatagram(data)
            self.updateTagInfo()
            self.repaint()

    def processDatagram(self, datagram):
        if datagram["Packet type"] == "tag":
            if not (datagram["tag_id"] in self.tag_dict):
                self.addTag(Tag(datagram["tag_id"]))
            self.tag_dict[datagram["tag_id"]].updateProperties(datagram)
            number_of_anchors = len(datagram["anchor_times"])
            self.positioning_window.eventlog_listwidget.insertItem(0, "Tag " + hex(datagram["tag_id"]) + " blink received by " + str(number_of_anchors) + " anchors" )


class PositioningWindow(QMainWindow):
    def __init__(self, queue,  parent = None):
        super(PositioningWindow, self).__init__(parent)
        self.parent = parent

        #create subwindow with tag list
        self.tag_list_dockwindow = QDockWidget("Tags", self)
        self.addDockWidget(Qt.RightDockWidgetArea, self.tag_list_dockwindow)
        self.tag_list_dockwindow.setFloating(False)

        self.tag_listwidget = QListWidget()
        self.tag_listwidget_items = {}
        self.tag_list_dockwindow.setWidget(self.tag_listwidget)

        #create subwindow with tag info
        self.tag_info_dockwindow = QDockWidget("Tag info", self)
        self.addDockWidget(Qt.RightDockWidgetArea, self.tag_info_dockwindow)
        self.tag_info_treewidget = QTreeWidget()
        self.tag_info_treewidget.setHeaderHidden(True)
        self.tag_info_treewidget_items = {}
        self.tag_info_dict = OrderedDict()
        self.tag_info_dict["Tag ID"] =  0x0000;  self.tag_info_dict["Position (mm)"] = [0, 0, 0]; self.tag_info_dict["Acceleration"]= [0, 0, 0];
        self.tag_info_dict["Position std"] = 0; self.tag_info_dict["Update rate"] = 0; self.tag_info_dict["Packet loss"] = 0;
        self.tag_info_dict["RSS"] = 0; self.tag_info_dict["Anchor timestamps"] = 0
        for name, value in self.tag_info_dict.items():
            name_node = QTreeWidgetItem()
            name_node.setText(0, name)
            self.tag_info_treewidget.addTopLevelItem(name_node)
            self.tag_info_treewidget_items[name] = QTreeWidgetItem()
            self.tag_info_treewidget_items[name].setText(0, str(value))
            name_node.addChild(self.tag_info_treewidget_items[name])
        self.tag_info_treewidget.expandAll()
        self.tag_info_dockwindow.setWidget(self.tag_info_treewidget)
        self.tag_info_dockwindow.setFloating(False)

        #create subwindow with eventlog info
        self.eventlog_dockwindow = QDockWidget("Event log", self)
        self.addDockWidget(Qt.BottomDockWidgetArea, self.eventlog_dockwindow)
        self.eventlog_dockwindow.setFloating(False)
        self.eventlog_listwidget = QListWidget()
        self.eventlog_dockwindow.setWidget(self.eventlog_listwidget)



        self.location_map_widget = Localisation(queue, self)
        self.setCentralWidget(self.location_map_widget)



class Tag():
    def __init__(self, tag_id = 0x0000, position = [0, 0, 0]):
        self.id = tag_id
        self.is_visible = False

        self.properties = {}
        self.properties["Position (mm)"] = position
        self.properties["Acceleration"] = [0, 0, 0]
        self.properties["Position std"] = 0
        self.properties["Update rate"] = 0
        self.properties["RSS"] = 0
        self.properties["Packet loss"] = 0
        self.properties["Anchor timestamps"] = {"Test": 1, "Bla": 2}

    def updateProperties(self, datagram):
        self.properties["Position (mm)"] = [datagram["x"], datagram["y"], datagram["z"]]
        self.properties["Acceleration"] = [datagram["a_x"], datagram["a_y"], datagram["a_z"]]
        self.properties["Position std"] = datagram["sigma"]
        self.properties["Update rate"] = datagram["update_rate"]
        self.properties["RSS"] = datagram["rss"]
        self.properties["Packet loss"] = datagram["packet_loss"]
        self.properties["Anchor timestamps"] = datagram["anchor_times"]

    def setVisible(self, is_checked):
        if(is_checked == 0):
            self.is_visible = False
        else:
            self.is_visible = True


class Localisation(QWidget):

    def __init__(self, queue, parent):
        super(Localisation, self).__init__()
        self.parent = parent
        self.offset_x = 10
        self.offset_y = 10
        self.anchors_dict = parent.parent.anchor_dict

        self.tag_size = 100
        self.map_size_x =  8369
        self.map_size_y = 5614
        self.mm_per_grid = 1000
        self.num_gridlines_x = int(self.map_size_x / self.mm_per_grid) + 1
        self.num_gridlines_y = int(self.map_size_y / self.mm_per_grid) + 1

        self.pixels_per_mm = min(self.size().width()/ self.map_size_x, self.size().height()/ self.map_size_y)


        self.previous_time = 0

        self.cowImage = QImage()
        self.cowImage.load("fig/cow.png")

    def resizeEvent(self, event):
        self.pixels_per_mm = min(self.size().width()/ self.map_size_x, self.size().height()/ self.map_size_y)

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.updateLocations(qp)
        qp.end()

    def updateLocations(self, qp):

        size = self.size()
        qp.setPen(QPen(QBrush(Qt.red), 5))
        self.drawGrid(qp)
        qp.setPen(Qt.red)


        qp.setBrush(Qt.red)
        for tag_id, tag in self.parent.parent.tag_dict.items():
            if tag.is_visible:
                x = tag.properties["Position (mm)"][0] * self.pixels_per_mm
                y = size.height() -  tag.properties["Position (mm)"][1] * self.pixels_per_mm
                rect = QRect(x - self.tag_size/2, y - self.tag_size/2, self.tag_size, self.tag_size)
                qp.drawImage(rect, self.cowImage)

    def drawGrid(self, qp):
        size = self.frameGeometry()
        increment = int(self.mm_per_grid * self.pixels_per_mm)
        qp.setPen(QPen(QBrush(Qt.black), 1))
        # vertical
        for i in range(0, self.num_gridlines_x):
            qp.drawLine(i * increment, self.map_size_y * self.pixels_per_mm  ,  i * increment, 0)
        # horizontal
        for i in range(0, self.num_gridlines_y):
            qp.drawLine(0, self.map_size_y * self.pixels_per_mm - i * increment, self.map_size_x * self.pixels_per_mm, self.map_size_y * self.pixels_per_mm - i * increment)

        qp.setPen(QPen(QBrush(Qt.black), 6))
        qp.drawRect(0,0,self.map_size_x * self.pixels_per_mm, self.map_size_y * self.pixels_per_mm)


class DiagnosticsWindow(QMainWindow):
    def __init__(self, queue,  parent = None):
        super(DiagnosticsWindow, self).__init__(parent)

        #create subwindow with tag list
        self.tag_list_dockwindow = QDockWidget("Tags", self)
        self.addDockWidget(Qt.TopDockWidgetArea, self.tag_list_dockwindow)
        self.tag_list_dockwindow.setFloating(False)

        self.tag_listwidget = QListWidget()
        self.tag_listwidget_items = {}
        self.tag_list_dockwindow.setWidget(self.tag_listwidget)

        #create subwindow with tag list
        self.anchor_list_dockwindow = QDockWidget("Anchors", self)
        self.addDockWidget(Qt.TopDockWidgetArea, self.anchor_list_dockwindow)
        self.anchor_list_dockwindow.setFloating(False)

        self.anchor_listwidget = QListWidget()
        self.anchor_listwidget_items = {}
        self.anchor_list_dockwindow.setWidget(self.anchor_listwidget)
        for anchor_id in parent.anchor_dict:
            self.anchor_listwidget_items[anchor_id] = QListWidgetItem(hex(anchor_id))
            self.anchor_listwidget.addItem(self.anchor_listwidget_items[anchor_id])
            self.anchor_listwidget_items[anchor_id].setCheckState(False)

        self.diagnostics_plot_widget = DiagnosticsPlot(self)
        self.setCentralWidget(self.diagnostics_plot_widget)


class DiagnosticsPlot(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent=None):
        fig = Figure()
        self.axes1 = fig.add_subplot(221)
        self.axes2 = fig.add_subplot(222)
        self.axes1 = fig.add_subplot(223)
        self.axes2 = fig.add_subplot(224)
        # We want the axes cleared every time plot() is called
        # self.axes1.hold(False)
        # self.axes2.hold(False)

        FigureCanvas.__init__(self, fig)
        self.update_figure()

        # timer = QTimer(self)
        # timer.timeout.connect(self.update_figure)
        # timer.start(update_time_ms)

    def update_figure(self):
        self.axes1.cla()
        self.axes2.cla()
        colors = ['r', 'g', 'b']
        times = np.arange(0, 10)
        distances = times * times
        self.axes1.plot(times, distances)
        self.axes2.plot(times, distances)
        self.draw()
