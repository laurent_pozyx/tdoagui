# GUI for the TDOA demo.

## Documentation

### packets.json definitions

```json
"PACKAGE_NAME" : {
  "id" : id_number,
  "data" : [
    "anchor_id" : anchor_id_number,
    ...
  ],
},
```

### original meeting content

```
TDOA line-up 03/02
------------------

- Laurent wordt betrokken om iets te kunnen visualiseren via een GUI

System requirements:
 ______________________
 |     ______________ |
_|_   _|_          _|_|_      _____             _____
|_|   |_|          | S |      | S |             |   |
		           | W |__UDP_| E |_DIAGNOSTICS_| G |
		           | I |      | R |             | U |
		           | T |_UDP__| V |___POSITION__| I |
		           | C |      | E |             |   |       
___   ___	       |_H_|      |_R_|             |___|
|_|   |_|	        | | 	    |
 |     |____________| |		    |___ EVENTUEEL SAVEN
 |____________________|

- UDP pakketen gaan van anchors via switch naar 'posisitoning server'
- UDP pakketen komen in 4 flavours (RX diagnostics steeds vooraan indien aanwezig, wordt aangegeven met bit in header)
	- sent beacon:  (eigen) anker ID, timestamp, beacon index
                              16 bit	   16 bit     8 bit
	- received beacon: (eigen) anker ID, TX anker ID, timestamp, RX diagnostics (decaWave struct, inclusief RSS), beacon index, beacon payload (debug)
				16 bit	     16 bit        64 bit      8 x 16 bit                                      8 bit
	- received blink: (eigen) anker ID, TX tag iD,  timestamp, RX diagnostics, blink index, blink payload (bv accelerometer)
				16 bit	     16 bit      64 bit      8 x 16 bit     8 bit
	- TWR: (eigen) anker ID, TX anker ID, range (mm), RSS (dB)
                        16 bit    16 bit       32 bit
- UDP header: FW versie, pakket type, bitfield data
- positioning server gaat:
	- positioneringsalgortime draaien
	- de sync coordineren
	- diagnostics runnen
	- positie en diagnostics naar een GUI doorsluizen
	- optie tot wegschrijven
- diagnostics bestaan uit:
	- RSS van tag naar anker
	- debiet (pakketten/ anker/ sec)
	- 'maat van hoe gesunchroniseerd het systeem is'
	- error messages vanuit de positioning en synchronisatie
- timing:
	- 1 beacon per superframe
	- verwerking van een blink gebeurt bij het ontvangen van een volgende blink
	- blinks worden gescheduled via hard coded slots
```
